# Hackathon (Jan 2016)

Grand Plan
===========

* Classical MD
  * Potentials (Rob, Simon)
  * Integrator and Thermostat (Clem, Tim, Kaito)
    * Euler, VV
    * energy conservation
    * exact results for H.O.
  * Static values (Casper, Felix)
    * Autocorrelation functions
  * Analysis & Visualization (pending ...)
  * Analytic Theory (Fred, Takeshi)


Usage
=====

To run MD in a 2D harmonic potential, run

    code/md.py

This will run an MD simulation with a very simple thermostat.

The RPMD bead force routines can be found in `code/potentials.py`

Theory
======

See tex-notes/morningtheorytalk.pdf for the RPMD notes from Wednesday morning as 
lectured by Rob Arbon.
