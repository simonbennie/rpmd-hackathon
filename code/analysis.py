import numpy as np
#import matplotlib.mlab as mlab
#import matplotlib.pyplot as plt
#import hamiltonian

def Analysis(p, q, energy, n_part):
    """ runs the ENTIRE analysis """


    # get some properties, n_part2 might be the number
    # of particles
    n_timesteps, n_dim, n_part2 = np.shape(p)

    n_beads = n_part2/n_part
    print("")
    print(" --------- ANALYSIS ---------- ")
    print("number of timesteps:", n_timesteps)
    print("number of particles:", n_part)
    print("number of beads: ", n_beads)
    print("dimensionality:", n_dim)
    # put data in form x[timestep,dimensions,particle,bead]
    positions , momenta = TransformData(p,q,n_beads,n_part)
    #Centroid(Time,Dimension,Particle)
    centroids = GetCentroids(positions)
    print(MeanSquaredCentroid(centroids))

def GetCentroids(positions):
    # Take a list of bead positions and 
    # calculate the cetroid position
    #Centroid(Time,Dimension,Particle)
    n_timesteps = positions.shape[0]
    n_dims = positions.shape[1]
    n_part = positions.shape[2]
    n_beads = positions.shape[3]
    centroid = np.zeros([n_timesteps,n_dims,n_part])
    for time in range(0,positions.shape[0]):
        for dim in range(0,positions.shape[1]):
        	for particle in range(0,n_part):
        		centroid[time,dim,particle] = (np.sum(positions[time,dim,particle,:]))/(n_beads)
    return(centroid)

def TransformData(p,q,n_beads,n_part):
	n_timesteps = q.shape[0]
	n_dims = q.shape[1]
	pTransformed = np.zeros([n_timesteps,n_dims,n_part,n_beads])
	qTransformed = np.zeros([n_timesteps,n_dims,n_part,n_beads])
	for time in range(0,n_timesteps):
		for dim in range(0,n_dims):
			for particle in range(0,n_part):
				pTransformed[time,dim,particle,:] = p[time,dim,particle*n_beads:(particle+1)*n_beads]
				qTransformed[time,dim,particle,:] = q[time,dim,particle*n_beads:(particle+1)*n_beads]
	return(pTransformed,qTransformed)

def MeanSquaredCentroid(centroids):
	mscentroid = np.zeros([centroids.shape[1],centroids.shape[2]])
	for particle in range(0,centroids.shape[2]):
		for dim in range(0,centroids.shape[1]):
			mscentroid[dim,particle] = np.mean(centroids[:,dim,particle]**2)
	return(mscentroid)

#def GetPositionProbabilityDistribution(centroids):
#    n, bins, patches = plt.hist(centroids, 2, normed=1, facecolor='green', alpha=0.75)
    # add a 'best fit' line
#    y = mlab.normpdf( bins, mu, sigma)
#    l = plt.plot(bins, y, 'r--', linewidth=1)
#    plt.xlabel('Smarts')
#    plt.ylabel('Probability')
#    plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
#    plt.axis([40, 160, 0, 0.03])
#    plt.grid(True)
#    plt.show()

#def Analyse(r,v,timestep):
#    # r()
#    rMatrix(timestep,:,:) = r
#    rMatrix(timestep,:,:) = v



def CorrelationFunction(A,B):
    #TO DO: The energy at each time needs to be added
    #Max value of t is equal to the length of the time series A and B
    tMax = len(A)
    #Prefactor  1/(Q*(2pihbar)^n)
    PartitionFunction = 1 #Need accurate Partition function
    n_beadss = 1
    Prefactor = 1/(PartitionFunction*(2*3.14159)**n_beadss)
    #IntialConditions
    time0 = 0
    timet = 1
    #The correlation function 
    qcft = np.zeros(tMax)
    for timet in range (1,tMax):
        qcftSum = 0
        NInitialTimes = tMax-timet
        for time0 in range(0,NInitialTimes):
            #Need access to energy at time0

            qcftSum = qcftSum + A[time0]*B[time0+timet]
        qcft[timet] = qcftSum/NInitialTimes
    return(qcft)

def DeKubofyCorrelationFunction(KTCF):
    # Converts Kubo Transformed Correlation function to Real Time Correlation function
    # TEMPORARILY FUDGED PARAMETERS
    Beta = 1
    FTKTCF = np.fft.fft(KTCF)
    freqs = np.fft.fftfreq(KTCF.shape[-1])
    FTRealTimeCF = np.zeros(len(KTCF),np.complex64)
    for i in range(0,len(KTCF)):
        if freqs[i] == 0:
            FTRealTimeCF[i] = Beta*FTKTCF[i] 
        else:
            FTRealTimeCF[i] = (Beta*freqs[i])/(1-np.exp(-Beta*freqs[i]))*FTKTCF[i] 

    RealTimeCF = np.fft.ifft(FTRealTimeCF)
    return(RealTimeCF)

def ReKubofyCorrelationFunction(RTCF):
    # Converts Kubo Transformed Correlation function to Real Time Correlation function
    # TEMPORARILY FUDGED PARAMETERS
    Beta = 1
    FTRTCF = np.fft.fft(RTCF)
    freqs = np.fft.fftfreq(RTCF.shape[-1])
    FTKuboTransformedCF = np.zeros(len(RTCF),np.complex64)
    for i in range(0,len(RTCF)):
        if freqs[i] == 0:
            FTKuboTransformedCF[i] = (1/Beta)*FTRTCF[i]
        else:
            FTKuboTransformedCF[i] = (1-np.exp(-Beta*freqs[i]))/(Beta*freqs[i])*FTRTCF[i]
    KTCF = np.fft.ifft(FTKuboTransformedCF)
    return(KTCF)


if __name__ == '__main__':
    #global NParticles
    #global n_beadss
    #global NDimensions
    #global NTimesteps
    #global kb
    #global BetaN
    #BetaN = 1/(kb*Temp*n_beadss)
    #kb = 1.38*10^-23
    #CorrelationFunction(np.sin(np.linspace(0,10*np.pi,100)),np.sin(np.linspace(0,10*np.pi,100)))
    


    # ------------------------------- TESTS -----------------------------------
    #De and Re Kubofication Test
    RTCF = DeKubofyCorrelationFunction(np.sin(np.linspace(0,10*np.pi,100)))
    KTCFNew = ReKubofyCorrelationFunction(RTCF)
    assert np.abs(np.sum(np.sin(np.linspace(0,10*np.pi,100))-KTCFNew))<10e-10
    # -------------------------------------------------------------------------




