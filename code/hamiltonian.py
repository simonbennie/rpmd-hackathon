from potentials import harmonic
import numpy

def H(p,q,m,V):
    """Evaluate the Hamiltonian for a set of positions (q), momenta (p), masses (m)

       TODO: fix for n beads

       p and q are vectors
       m is a vector of the masses of the particles
       V is the potential energy function

       see eq 9 in DOI: 10.1146/annurev-physchem-040412-110122
    """

    kin = p*p / (2.0 * m)
    pot, force = V(q, m)
    return numpy.sum(kin) + numpy.sum(pot)

def Qn(p_t,q_t,m,V,T):
    """ evaluates the partition function Qn

        see eq 8 in DOI: 10.1146/annurev-physchem-040412-110122

        TODO: make it "beta-n"
    """
    # beta = 1 / k_B T
    k_B = 1.0
    beta = 1.0 / (k_B * T)
    n_timesteps, _ = numpy.shape(p_t)
    print("Qnt: n_timesteps = {0:3d}".format(n_timesteps))

    Qnt = numpy.zeros(n_timesteps)
    for i,(p,q) in enumerate(zip(p_t, q_t)):
        Qnt[i] = -beta*H(p,q,m,V)

    Qnt = numpy.exp(Qnt)
    return numpy.sum(Qnt)

if __name__ == '__main__':
    # dummy data. 1 particle, 4 timesteps
    Q_t = numpy.array([[0.0],[0.1], [0.15], [0.1]])
    P_t = numpy.array([[0.1], [0.05], [0.025], [-0.05]])
    masses = numpy.array([1.0])

    print Qn(Q_t, P_t, masses, harmonic, 1.0)
