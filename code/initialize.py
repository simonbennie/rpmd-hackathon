import numpy as np

from forces import HarmonicForceAndPotEnergy, KineticEnergy

def InitialPosVel(Energy, Mass, Freq,omega_n, n_bead):

  npart,ndim=len(Mass),len(Freq)
  Ken=np.random.uniform(0,Energy, npart)
  Pot=Energy-Ken
  print ("Ken",Ken,"Pot",Pot)
  rrFreqWeight = np.sqrt( Pot/(0.5*Mass)  )
  vv = np.sqrt( Ken/(0.5*Mass))

  if ndim==2:
    theta1 = np.random.uniform(0,2*np.pi, npart ) # position
    theta2 = np.random.uniform(0,2*np.pi, npart ) # Velocity
    print (theta1)
    Pos = np.array( [ rrFreqWeight*np.cos(theta1)/Freq[0],
                      rrFreqWeight*np.sin(theta1)/Freq[1]  ] )
    Vel = np.array( [ vv*np.cos(theta2), vv*np.sin(theta2)  ] )
  elif ndim==1:
    sign1 = np.random.randint(0,2)*2-1
    sign2 = np.random.randint(0,2)*2-1
    Pos = np.array( [ rrFreqWeight*sign1/Freq[0] ] )
    Vel = np.array( [ vv*sign2  ] )

  else:
    raise NotImplementedError("Ndim {} not implemented".format(ndim))

  assert Pos.shape==(ndim,npart )
  assert Vel.shape==(ndim ,npart)
  Kenactual = KineticEnergy(Vel,Mass)
  _, potenactual = HarmonicForceAndPotEnergy(Pos,Mass, Freq, omega_n, n_bead)
  assert abs(Ken.sum()-Kenactual)<1.e-7
  assert abs(potenactual-Pot.sum())<1.e-7

  return Pos, Vel
