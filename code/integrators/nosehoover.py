#!/usr/bin/env python
#-*- coding:utf-8 -*-

class NoseHoover(object):
  """Implementation of Nose Hoover thermostat from Appendix E of Frenkel and
  Schmidt, Understanding Molecular Simulations.

  Typed up by Tim Wiles, 20150603"""

  def __init__(self):
    # TODO initialize the parameters
    pass

  def integrate(self):
    chain()
    posvel()
    chain()

  def chain(self):
    """Apply equation E.2.5"""
    deltat = self.deltat
    N,d = self.atompos.shape
    L = d*N  # we use "real" time, so L=d*N

    Q1 = self.Q1
    atompos = self.atompos
    atomvel = self.atomvel

    G2 = (Q1 * self.xi1vel**2 - temperature)/Q2
    self.xi2vel += G2*deltat/4                      # using equation (E.2.6)
    self.xi1vel *= numpy.exp(-self.xi2vel*deltat/8) # using equation (E.2.7)
    G1 = (2*self.kineticen - L*temperature)/Q1
    self.xi1vel += G1*deltat/4                      # using equation (E.2.8)
    self.xi1vel *= numpy.exp(-self.xi2vel*deltat/8) # using equation (E.2.7)
    self.xi1    += self.xi1vel*deltat/2             # using equation (E.2.9) #TODO CHECK
    self.xi2    += self.xi2vel*deltat/2             # using equation (E.2.10)
    s = numpy.exp(- self.xi1vel*deltat/2) # Scale factor in equation (E.2.11)
    self.atomvel[:,:] *= s                          # using equation (E.2.11)
    self.kineticen    *= s**2                       # scale kinetic energy
    self.xi1vel *= numpy.exp(-self.xi2vel*deltat/8) # using equation (E.2.7)
    G1 = (2*self.kineticen - L*temperature)/Q1
    self.xi1vel += G1*deltat/4                      # using equation (E.2.8)
    self.xi1vel *= numpy.exp(-self.xi2vel*deltat/8) # using equation (E.2.7)
    G2 = (Q1 * self.xi1vel**2 - temperature)/Q2
    self.xi2vel += G2*deltat/4                      # using equation (E.2.6)

  def posvel(self):
    """Apply eqn E.2.4"""
    deltat = self.deltat

    self.atompos += self.atomvel*deltat/2           # using equation (E.2.13)
    forces = self.calcforces()
    self.atomvel += forces*deltat/self.masses       # using equation (E.2.12)
    self.atompos += self.atomvel*deltat/2           # using equation (E.2.13)
    self.kineticen = (self.masses*self.atomvel**2/2).sum()

def noseHooverInit():
  nh = NoseHoover()
  nh.calcforces = CalculateForcesfunction
  while True:
    NoseHooverstep(nh, Pos,Vel,tStep,Mass,Force_fn, Freq, 
                                    OldForce=Force )

def NoseHooverstep(self, Pos,Vel,tStep,Mass,Force_fn, Freq, 
                                    OldForce=None):
  
  nh.integrate()
  Pos, Vel, Force = nh.atompos, nh.atomvel, forces
  # TODO implement nose hoover step

