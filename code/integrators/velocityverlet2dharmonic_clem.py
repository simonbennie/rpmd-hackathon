#!/usr/bin/python2.7
#-*- coding:utf-8 -*-

from __future__ import division, print_function


from Thermostat import *
from DrawPos import DrawPos

import sys
import numpy as np

import matplotlib.pyplot as plt

def HarmonicForceAndPotEnergy(pos,mass, Freq, centre=None):
#  import ipdb; ipdb.set_trace()
  if centre is None: 
    centre = np.zeros(pos.shape)
  force = -mass[None,:]*Freq[:,None]**2*pos[:,:] 
  potenergy = (0.5*mass[None,:]*
                Freq[:,None]**2*(pos[:,:]-centre)**2
                                  ).sum()
  return force, potenergy
#######################################################
#######################################################
#######################################################

def KineticEnergy(Vel,Mass):
  return np.sum(0.5*Mass[None,:]*Vel[:,:]**2)
 
#######################################################
#######################################################
#######################################################

def VelocityVerletStep(Pos,Vel,tStep,Mass,Force_fn, Freq, 
                                    OldForce=None):

  assert Pos.shape==Vel.shape
  assert (Pos.shape[1],)==Mass.shape
  assert (Pos.shape[0],)==Freq.shape

  if OldForce is None:
    OldForce, _ =Force_fn(Pos,Mass, Freq)
    print (OldForce)

  NewPos=Pos+(Vel*tStep)+(0.5*(OldForce/Mass[None,:])*(tStep**2))

  Force,PotEnergy=HarmonicForceAndPotEnergy(NewPos,Mass, Freq)
  KenEnergy=KineticEnergy(Vel,Mass)


  # change
  NewForce, _ =Force_fn(NewPos,Mass, Freq)

  NewVel=Vel + (0.5*tStep*(OldForce+NewForce)/Mass[None,:])

  return (NewPos, NewVel, NewForce)
#######################################################
#######################################################
#######################################################

# def NoseHooverstep(nh, Pos,Vel,tStep,Mass,Force_fn, Freq, 
#                                     OldForce=None):
#   # TODO implement nose hoover step
#   nh.calcforces = 

#######################################################
#######################################################
#######################################################
def InitialPosVel(Energy=1, Mass=None, Freq=None,):
  if Mass is None:
    Mass = np.ones(1)
  if Freq is None:
    Freq = np.ones(2)

  npart,ndim=len(Mass),len(Freq)
  Ken=np.random.uniform(0,Energy, npart)
  Pot=Energy-Ken
  print ("Ken",Ken,"Pot",Pot)
  rrFreqWeight = np.sqrt( Pot/(0.5*Mass)  )
  vv = np.sqrt( Ken/(0.5*Mass))

  if ndim==2:
    theta1 = np.random.uniform(0,2*np.pi, npart ) # position
    theta2 = np.random.uniform(0,2*np.pi, npart ) # Velocity
    print (theta1)
    Pos = np.array( [ rrFreqWeight*np.cos(theta1)/Freq[0],
                      rrFreqWeight*np.sin(theta1)/Freq[1]  ] )
    Vel = np.array( [ vv*np.cos(theta2), vv*np.sin(theta2)  ] )
    Kenactual = KineticEnergy(Vel,Mass)
    _, potenactual = HarmonicForceAndPotEnergy(Pos,Mass, Freq)
    assert abs(Ken-Kenactual)<1.e-7
    assert abs(potenactual-Pot)<1.e-7
  else:
    raise NotImplementedError("Ndim {} not implemented".format(ndim))
 
    

  return Pos, Vel
  
#######################################################
#######################################################
#######################################################
def main():

  Mass=np.ones(1)
  Freq=np.ones(2)
   
  
  
  np.random.seed(10)
  Pos, Vel = InitialPosVel()


  mmmax = 20000
  nStepsRescale=1000
  nStepSave=1
  tStep = 0.01
  
  PosList=np.zeros(shape=(Pos.shape[0],Pos.shape[1],0))
  VelList=np.zeros(shape=(Vel.shape[0],Vel.shape[1],0))
  EnergyList=np.zeros(shape=(0))
  
  Force = None
  for mm in range(1,mmmax):

    Force,PotEnergy=HarmonicForceAndPotEnergy(Pos,Mass, Freq)
    Pos, Vel, Force = VelocityVerletStep(Pos,Vel,tStep,Mass,HarmonicForceAndPotEnergy, Freq,OldForce=Force)
    KenEnergy=KineticEnergy(Vel,Mass)
    
    VelList=np.dstack((VelList,Vel))
    PosList=np.dstack((PosList,Pos))
    EnergyList=np.hstack((EnergyList,KenEnergy+PotEnergy))
    
#     if mm%nStepsRescale==0:
    if mm>nStepsRescale:
      Force,PotEnergy=HarmonicForceAndPotEnergy(Pos,Mass, Freq)
#       print (mm)
#       print ("Energy before Thermostat is", PotEnergy+KenEnergy, "KE before Thermostat is",KenEnergy)
    	
      
      Vel=Vel*VelocityRescaleThermostat(VelList[:,:,-nStepsRescale:-1],Mass,0.1)
      ThermVelList=np.zeros(shape=(Vel.shape[0],Vel.shape[1],0))
      
      KenEnergy=KineticEnergy(Vel,Mass)
#       print ("Energy after Thermostat is", PotEnergy+KenEnergy, "KE before Thermostat is",KenEnergy)
#       print ()

  PlotTemp(VelList,Mass,mmmax*tStep)
  plt.hist(EnergyList[10*nStepsRescale:], 50, normed=1, facecolor='green', alpha=0.75)
  plt.show()
  plt.close()
  
  plt.plot(np.linspace(0,mmmax*tStep,mmmax-1),EnergyList)
  plt.show()
#######################################################
#######################################################
#######################################################
if __name__ == '__main__':
  main()


