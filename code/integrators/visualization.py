#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import pygame
import numpy as np
import sys


# -------------- make the colors --------------------------
def hexcolor(code):
  return int(code[0:2],16),int(code[2:4],16),int(code[4:6],16)

colors = []
colors.append( ( 255, 0, 0) ) # red
colors.append( ( 0, 255, 0) )
colors.append( ( 0, 0, 255))
colors.append( hexcolor("FFFF00")) # yello
colors.append( hexcolor("FF0099"))
morecolors = "800000,808000,008000,00FFFF,FFFFFF".split(",")
colors = colors + [ hexcolor(i) for i in  morecolors]
# -----------end make the colors --------------------------



def drawatoms(windowSurfaceObj,atompos, distancescale, radius, centre ):

  windowSurfaceObj.fill((255,255,255))

  #for i,atomspr in enumerate(atomsprites):
  for atomno in range(atompos.shape[1]):
    circlecentre = (atompos[:,atomno]+centre) *distancescale
    circlecentre = int(circlecentre[0]),int(circlecentre[1])
    pygame.draw.circle(windowSurfaceObj,
                        colors[atomno],circlecentre,int(radius),1)


###########################################################################
###########################################################################
###########################################################################

def listenforexit(events):
  """Check to see if the user quit"""
  for event in events:
    if event.type == pygame.QUIT:
      # change the value to False, to exit the main loop
      pygame.quit()
      sys.exit()

###########################################################################
###########################################################################
###########################################################################

class Visualizer(object):
  
  def __init__(self,windowwidth = 480, boxsize = 6, radius=0.2):
    # -------------- initialize the pygame module ------------------------
    self.distancescale = windowwidth / boxsize
    pygame.init()
    self.boxsize = boxsize
    self.radius = radius
    pygame.display.set_caption("CCC hackathon RPMD")
    self.windowSurfaceObj = pygame.display.set_mode((windowwidth,windowwidth)) 
    # -----------end initialize the pygame module ------------------------ 


  def update(self, Pos):

    events = pygame.event.get()
    listenforexit(events)

    drawatoms(self.windowSurfaceObj,Pos, self.distancescale, 
        radius=self.radius*self.distancescale,centre=(self.boxsize/2.,self.boxsize/2.) )
    pygame.display.update()


###########################################################################
###########################################################################
###########################################################################

if __name__ == '__main__':
  main()




