    Mass=np.ones(3)
    Freq=np.ones(2)
     
    np.random.seed(10)
    Pos, Vel = InitialPosVel(Energy=1, Mass=Mass, Freq=Freq )

    tStep = 0.001
    Force = None



      for step in range(stepsperframe):
        Pos, Vel, Force = VelocityVerletStep(
            Pos,Vel,tStep,Mass,HarmonicForceAndPotEnergy, 
                     Freq,     OldForce=Force)


      Force, PotEnergy = HarmonicForceAndPotEnergy(Pos,Mass, Freq)
      KenEnergy = KineticEnergy(Vel,Mass)
  #    print ("Energy is", PotEnergy+KenEnergy )
