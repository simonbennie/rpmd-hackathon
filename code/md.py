#!/usr/bin/python2.7
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import sys

import numpy as np

from potentials import force
from thermostat import VelocityRescaleThermostat
from thermostat import StocasticThermostat
from veloverlet import VelocityVerletStep
from initialize import InitialPosVel
from forces import HarmonicForceAndPotEnergy, KineticEnergy

from analysis import Analysis

try:
    import pygame
except ImportError:
    VISUALIZE = False
else:
    VISUALIZE = True
    import integrators.visualization


def MD(mmmax, nStepsStored, npart, n_beads, temperature, ndim=2, randseed=10, visualizeevery=None):
    """ Runs the MD
            
            mmmax = None to run forever
    """

    Mass=np.ones(npart*n_beads)
    Freq=np.ones(ndim)
    omega_n = n_beads*temperature
    
    np.random.seed(randseed)
    Pos, Vel = InitialPosVel(temperature/1, Mass, Freq,omega_n, n_beads)


    nStepsRescale=300
    tStep = 0.01
    assert mmmax >= nStepsStored, "Needs more MMMAX steps"
    
    PosList=np.zeros(shape=(nStepsStored,Pos.shape[0],Pos.shape[1]))
    VelList=np.zeros(shape=(nStepsStored,Vel.shape[0],Vel.shape[1]))
    EnergyList=np.zeros(nStepsStored)

    
    if visualizeevery is not None:
        vis = integrators.visualization.Visualizer()

    Force = None
    
    mm=0
    while True:
        if mmmax is not None and    mm>mmmax: break

        Pos, Vel, Force = VelocityVerletStep(Pos, Vel, tStep, Mass, 
            HarmonicForceAndPotEnergy, Freq, omega_n, n_beads, OldForce=Force)
        KenEnergy=KineticEnergy(Vel,Mass)
        
        VelList[mm%nStepsStored,:,:] = Vel
        PosList[mm%nStepsStored,:,:] = Pos
        KenEnergy=KineticEnergy(Vel,Mass)
        _,PotEnergy=HarmonicForceAndPotEnergy(Pos,Mass, Freq, omega_n, n_beads)
        EnergyList [mm%nStepsStored] = KenEnergy+PotEnergy
 
        if mm>nStepsStored:
            if visualizeevery is not None and mm % visualizeevery==0:
                vis.update(Pos)
 
        #Thermostat
        RandThermoNum=np.random.uniform(0,1,len(Mass))
        for partical in range(0,len(RandThermoNum),1):
            if RandThermoNum[partical]<(1.0/nStepsRescale):
                 Vel[:,partical]=StocasticThermostat(Mass[partical],temperature,ndim)

        mm+=1

    return PosList, VelList, EnergyList


#######################################################
#######################################################
#######################################################
if __name__ == '__main__':
    # some settings
    n_md_steps  = 60000
    n_particles = 1
    n_steps_to_store = 60000
    temperature = 2.0
    n_beads = 32
    Q, P, E = MD(n_md_steps, n_steps_to_store, n_particles, 
                 n_beads, temperature, ndim=1, randseed=None)
    Analysis(Q, P, E, n_particles)
    from matplotlib import pyplot
#     pyplot.hist( np.ndarray.flatten(Q), 50, normed=True )
#     pyplot.plot( np.arange(-1,1,0.1),
#                  
#                )
    pyplot.plot( np.arange(0,5,0.1),
#                  (2*3.1415*temperature) * 
                 np.exp( -np.arange(0,5,0.1) / temperature ),  )
    pyplot.hist( E, 50, normed=True )
    pyplot.show()

#     pyplot.plot(E)
#     pyplot.show()
    
    
