import numpy as np

from md import MD

temperatures = [0.001, 0.1, 0.2, 0.3, 0.5, 1.0, 2.0, 4.0, 10.0]
#temperatures = [0.001, 0.1, 0.2]
msq_displ = []

for temperature in temperatures:

    n_md_steps  = 50000
    n_particles = 1
    n_steps_to_store = 50000
    n_beads =1
    Q, P, E = MD(n_md_steps, n_steps_to_store,
                 n_particles, n_beads, temperature,
                 ndim=1, randseed=None)
    

    msq_displ.append( np.mean( Q*Q ) )

import matplotlib.pyplot as plt

f = plt.figure()
ax = f.add_subplot(111)
ax.plot(temperatures, msq_displ, linestyle='-', color='#000000', linewidth=2)
ax.set_xlabel(r"$T$")
ax.set_ylabel(r"$\langle x^2 \rangle$")

ax.set_xlim((0, temperatures[-1]))
ax.set_ylim((0, temperatures[-1]))

plt.show()
