import numpy as np

def force(positions, omega, mass):
	"""
	 
	"""
	potential, forces = harmonic(positions)
	forces  = forces + bead(positions, omega, mass)
	return forces, potential
 

def ext_harmonic(positions):
	k = 2.0
	potential = 0.5*k*positions**2
	potential = potential.sum(axis = 0)
	force = -k*positions
	return (potential, force)

def ext_double_well(positions):
	j = -0.5
	k = +0.25
	potential = j*positions**2 + k*positions**4
	potential = potential.sum(axis = 0)
	force = 2.0*j*positions + 4.0*k*positions**3
	return (potential, force)

def intra_bead(positions, mass_vector, omega, n):
	npart = len(mass_vector)/n
	ndim = positions.shape[0]
	assert positions.shape == (ndim, npart*n)
	bead_forces = np.zeros(positions.shape)
	
	if n > 1:

		positions = positions.T
		bead_forces = bead_forces.T
		bead_forces[0] = 2*positions[0] - positions[1] - positions[-1] 
		for i in range(1, len(positions)-1):
			x = (2*positions[i] - positions[i+1] - positions[i-1])
			bead_forces[i] = x

		bead_forces[-1] = 2*positions[-1] - positions[0] - positions[-2] 

		bead_forces = -(omega**2)*mass_vector*bead_forces.T

	return bead_forces

def inter_bead(positions, N, n, ndim, coupling):
	pass
	# x = positions.T.reshape(N, n, ndim)
	# forces = np.zeros(x.shape)
	# for i in range(N-1):
	# 	for j in range(i+1, N):













