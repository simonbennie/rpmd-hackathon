import numpy
import matplotlib
import matplotlib.pyplot as plt

from hamiltonian import H
from potentials import harmonic

"""
Static properties
  - average position <x>
  - average momentum <p>
  - average position squared <x^2>
  - average momentum squared <x^2>
"""

def energy(q):
   k = 1.0
   return 0.5 * k * q*q

def force(q):
   k = 1.0
   return - k * q

def integrate(q, p, f, dt):
    q_new = q + dt * p + 0.5 * dt * dt * f
    e_pot, f_new = harmonic(q_new)
    p_new = p + 0.5 * dt * (f_new + f)

    return q_new, p_new, f_new

def average(var):
   return numpy.mean(var)

def average_sq(var):
   return average( var * var )

X = numpy.array([2.0])
V = numpy.array([0.0])
F = force( X )

t_store = []
x_store = []
y_store = []
v_store = []

dt = 0.01
for k in range(1000):
    X, V, F = integrate(X, V, F, dt)
    t_store.append(k*dt)
    x_store.append(X[0])
    #y_store.append(H(V, X, numpy.array([1.0]), harmonic))
    v_store.append(V[0])
    #print("x = {0:6.2f}, v = {1:6.2f}, H(p, q) = {2:6.2f}".format(X[0], V[0], H(V, X, numpy.array([1.0]), harmonic)))

# some sickening mumbo jumbo y-axis fixes with automatic offsets
f = plt.figure() # figsize=(4,4))
ax = f.add_subplot(111)
y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
ax.yaxis.set_major_formatter(y_formatter)
ax.plot(t_store, x_store, marker='.', linestyle='')
ax.plot(t_store, v_store, marker='.', linestyle='')
ax.set_xlabel("$t$")
ax.set_ylabel("$A(t)$")

X_m = numpy.array(x_store)
V_m = numpy.array(v_store)
print average(X_m), average_sq(X_m)
print average(V_m), average_sq(V_m)

plt.show()
