#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function


import numpy as np


kb=1.0

def VelocityRescaleThermostat(Vel,Mass,Temp):
	
	kb=1.0
	AveKE=np.mean( 0.5*Mass[None,None,:]*(Vel**2).sum(axis=1) )
	ndim = Vel.shape[1]
	TargetKE=ndim*0.5*kb*Temp
	
	return ((((TargetKE/AveKE)**0.5)-1)/100)+1
	
def StocasticThermostat(Mass,Temp,nDim):
# 	NewVel=np.random.multivariate_normal(np.zeros(nDim),np.identity(nDim)*(kb*Temp)/(Mass))
	NewVel=np.random.normal(0,(kb*Temp/Mass)**0.5,nDim)
	return NewVel
	
	
def PlotTemp(Vel,Mass,tMax):
	
	KE=0.5*Mass[None,:]*np.linalg.norm(Vel,axis=0)**2
	Temp=KE[0]/(kb*Vel.shape[0]*0.5)
	
	print (np.linspace(0,tMax,Vel.shape[2]).shape, Temp.shape)
	
	
