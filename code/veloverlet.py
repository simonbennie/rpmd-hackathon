
def VelocityVerletStep(Pos,Vel,tStep,Mass,Force_fn, Freq, omega_n, n_bead, 
                                    OldForce=None):

  assert Pos.shape==Vel.shape
  assert (Pos.shape[1],)==Mass.shape
  assert (Pos.shape[0],)==Freq.shape

  if OldForce is None:
    OldForce, _ =Force_fn(Pos,Mass, Freq, omega_n, n_bead)

  NewPos=Pos+(Vel*tStep)+(0.5*(OldForce/Mass[None,:])*(tStep**2))

  # change
  NewForce, _ =Force_fn(NewPos,Mass, Freq, omega_n, n_bead)

  NewVel=Vel + (0.5*tStep*(OldForce+NewForce)/Mass[None,:])

  return (NewPos, NewVel, NewForce)
