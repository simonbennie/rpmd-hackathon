#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble
% \usepackage{setspace}
\let\Oldref\ref
\renewcommand{\ref}[1]{(\Oldref{#1})}
\usepackage{lscape}
\usepackage{framed,xcolor}
\definecolor{shadecolor}{gray}{0.9}
\usepackage{mdframed}

%%% Fix angstroms
\usepackage{siunitx}
\newcommand{\AAtim}{{\si{\angstrom}}}

%\usepackage{tablefootnote}

%\usepackage[symbol]{footmisc}

\usepackage{pbox}


%%%%%%%%%%%%%%% Sort headers and footers
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
%\renewcommand{\headrulewidth}{0pt}
\cfoot{Page \thepage\ of \pageref{LastPage}}
%%%%%% end sort headers and footeres


%%%%%%%%% identity matrix blackboard font (mathbbm)
\usepackage{bbm}
%%%%%%%%% end dentity matrix blackboard font

%%%%%%%%%%%% bold math symbols like sigma
\usepackage{bm}


%%%% fix the toc
\usepackage{tocloft}
\setlength\cftparskip{0pt}
\setlength\cftbeforesecskip{0pt}
\tocloftpagestyle{empty} % stop it changing the style

%%%% put boxes around floats
\usepackage[framestyle=fbox,framefit=yes,heightadjust=all,framearound=all]{floatrow}    

%%% stop floats going to the wrong places
\usepackage{placeins}
% Added by lyx2lyx
% Added by lyx2lyx
% Added by lyx2lyx

%% make all the kets the same height
%\newcommand{\keth}{\vphantom{a^\prime b^\prime c^\prime d^\prime i^\prime j^\prime k^\prime l^\prime } }
% Added by lyx2lyx
% Added by lyx2lyx
% Added by lyx2lyx
\usepackage{cancel}
\end_preamble
\options notitlepage
\use_default_options false
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman times
\font_sans helvet
\font_typewriter courier
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement !h
\paperfontsize 10
\spacing onehalf
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize a4paper
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 1cm
\rightmargin 1cm
\bottommargin 2cm
\headheight 1cm
\headsep 1cm
\footskip 1cm
\secnumdepth 0
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title

\lang english
CCC Ring Polymer Molecular Dynamics Hackathon
\end_layout

\begin_layout Part

\lang english
Morning theory talk from Fred and Rob Arbon (minutes)
\end_layout

\begin_layout Section

\series bold
\lang english
10:15
\end_layout

\begin_layout Standard

\lang english
Question from Fred: What is the IR spectrum (
\begin_inset Formula $\epsilon_{r}\left(\omega\right)$
\end_inset

) of a finite temperature  harmonic oscillator (quantum and classical)
\end_layout

\begin_layout Subsection

\series bold
\lang english
10:30
\series default
 
\end_layout

\begin_layout Standard

\lang english
Talk from Rob about RPMD
\end_layout

\begin_layout Standard

\lang english
\begin_inset Formula 
\begin{align*}
Q & =Tr\left[e^{-\beta\hat{H}}\right]\\
 & =\int d\mathbf{r}\,\left\langle \mathbf{r}|e^{-\beta\hat{H}}|\mathbf{r}\right\rangle \\
e^{-\beta\left(\hat{K}+\hat{V}\right)} & =\lim_{n\rightarrow\infty}\left[e^{-\frac{\beta}{2n}\hat{V}}e^{-\frac{\beta}{n}\hat{K}}e^{-\frac{\beta}{2n}\hat{V}}\right]^{n} & \text{Trotter's formula}\\
Q & =\lim_{n\rightarrow\infty}Q_{n}\\
Q_{n} & =\int d\mathbf{r}\,\left\langle \mathbf{r}|\left(e^{-\frac{\beta}{2n}\hat{V}}e^{-\frac{\beta}{n}\hat{K}}e^{-\frac{\beta}{2n}\hat{V}}\right)^{n}|\mathbf{r}\right\rangle \\
1 & =\left(\frac{\beta}{2\pi m}\right)^{\frac{3}{2}}\int d\mathbf{p}\, e^{-\frac{\beta p^{2}}{2m}} & \text{An identity}\\
Q_{n} & =\left(\frac{\beta}{2\pi m}\right)^{\frac{3}{2}n}\int\left(\prod_{k}d\mathbf{p}_{k}\right)\left(\prod_{k}d\mathbf{r}_{k}\right)\exp\left[\sum_{k=0}^{n-1}\left(\frac{p_{k}^{2}}{2m}+\frac{1}{2}m\omega_{n}^{2}\left(\mathbf{r}_{k}-\mathbf{r}_{\left(k+1\right)\%n}\right)^{2}\right)+V\left(\mathbf{r}_{k}\right)\right]\\
\omega_{n} & :=\frac{1}{\beta_{n}\hbar}\ \beta_{n}:=\frac{\beta}{n}.
\end{align*}

\end_inset


\end_layout

\begin_layout Section

\series bold
\lang english
10:30
\end_layout

\begin_layout Standard

\lang english
Note from Clem: as 
\begin_inset Formula $n\rightarrow\infty$
\end_inset

, the coupling gets stronger.
\end_layout

\begin_layout Section

\series bold
\lang english
10:40
\series default
 
\end_layout

\begin_layout Standard

\lang english
Question from Clem: What happens when 
\begin_inset Formula $T\rightarrow0$
\end_inset

 to the harmonic oscillator in RPMD?
\end_layout

\end_body
\end_document
